*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${url}                           http://exemple-docker-and-flask_flask-app_1:5000
${navegador}                     googlechrome
${remote_url}                    http://selenium-hub:4444/wd/hub
${initial_page}                  xpath=//h1
${input_name}                    xpath=//input[@name='name']
${input_email}                   xpath=//input[@name='email']
${button_add}                    xpath=//button[@type='submit']
${button_att}                    xpath=//button[@type='submit']
${button_edit}                   //li[contains(., 'meg')]//*[contains(., 'Editar')]
${button_delete}                 Excluir
${assert_cadastro}               xpath=//li[contains(., '')]


*** Keywords ***
Create Chrome Options
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    --start-maximized
    Call Method    ${chrome_options}    add_argument    --disable-infobars
    [Return]       ${chrome_options}

Abrir pagina
    ${chrome_options}=    Create Chrome Options
    Open Browser    ${url}    ${navegador}    remote_url=${remote_url}    options=${chrome_options}

Dado estou na pagina de cadastro
    # ${chrome_options}=    Create Chrome Options
    # Open Browser    ${url}    ${navegador}    remote_url=${remote_url}    options=${chrome_options}
    # Open Browser                 ${url}                ${navegador}
    Element Should Be Visible    ${initial_page}
    Sleep    1s
    Capture Page Screenshot

E que preencho os dados do usuario
    [Documentation]              Insere nome e e-mail nos campos de cadastro
    [Arguments]                  ${nome}               ${email}
    Sleep    1s
    Capture Page Screenshot
    Input Text                   ${input_name}         ${nome}
    Input Text                   ${input_email}        ${email}
    Sleep    1s
    Capture Page Screenshot

E clico no botao adicionar
    Click Button                 ${button_add}

E clico no botao atualizar
    Click Button                 ${button_att}

Entao verifico que o usuario foi cadastrado
    [Arguments]                  ${nome}
    ${assert_cadastro} =         Set Variable          xpath=//li[contains(., '${nome}')]
    Element Should Contain       ${assert_cadastro}    ${nome}
    Sleep    1s
    Capture Page Screenshot
    Close All Browsers

Quando clico em editar
    [Arguments]                  ${nome}
    ${button_edit} =             Set Variable          xpath=//li[contains(., '${nome}')]//*[contains(., 'Editar')]
    Click Link                   ${button_edit}
    Sleep    1s
    Capture Page Screenshot

Entao verifico que o usuario foi editado
    [Arguments]                  ${nome}
    ${assert_cadastro} =         Set Variable          xpath=//li[contains(., '${nome}')]
    Element Should Contain       ${assert_cadastro}    ${nome}
    Sleep    1s
    Capture Page Screenshot
    Close All Browsers

Quando clico em excluir
    [Arguments]                  ${nome}
    ${button_edit} =             Set Variable          xpath=//li[contains(., '${nome}')]//*[contains(., 'Excluir')]
    Click Link                   ${button_delete}
    
Entao verifico que o usuario foi excluido
    [Arguments]                  ${nome}
    ${assert_cadastro} =         Set Variable          xpath=//li[contains(., '${nome}')]
    Element Should Not Be Visible    ${assert_cadastro}
    Sleep    1s
    Capture Page Screenshot    
    Close All Browsers