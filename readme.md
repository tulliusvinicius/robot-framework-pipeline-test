# Projeto Robot Framework com Selenium Grid

Este repositório contém um projeto de automação de testes utilizando o Robot Framework em conjunto com o Selenium Grid. O projeto é composto por um script `robot_selenium_grid.robot`, um arquivo `requirements.txt` para instalar as dependências necessárias e um arquivo `Dockerfile` que cria uma imagem Docker com o script do Robot Framework.

## Estrutura do Projeto

- **robot_selenium_grid.robot:** Este arquivo contém o script do Robot Framework para os testes de automação.
- **requirements.txt:** Lista as dependências necessárias para o projeto.
- **Dockerfile:** Este arquivo é usado para criar uma imagem Docker contendo o script do Robot Framework e suas dependências.

## Configuração e Execução

### 1. Configurando o Selenium Grid

- **Criar uma rede:**
    ```bash
    docker network create grid

- **Executar imagem do Selenium-hub:**
    ```bash
    docker run -d -p 4442-4444:4442-4444 --net grid --name selenium-hub selenium/hub

- **Executar imagem do node-chrome:**
    ```bash
    docker run -d --net grid --name node_chrome_2 -e SE_EVENT_BUS_HOST=selenium-hub --shm-size="2g" -e SE_EVENT_BUS_PUBLISH_PORT=4442 -e SE_EVENT_BUS_SUBSCRIBE_PORT=4443 selenium/node-chrome


### . Executando em Containers Docker

#### a. Construindo a Imagem Docker

  Para criar uma imagem Docker com o script do Robot Framework e suas dependências, utilize o `Dockerfile` fornecido. No diretório raiz do projeto, execute o seguinte comando para construir a imagem Docker:

 -
    ```bash
    build -t robot-grid-poc:dev-0.0.1 .


#### b. Executando os Containers

Após construir a imagem Docker, você pode executar os dois containers necessários para o projeto. Certifique-se de que o container do Selenium Grid está em execução antes de executar o container com o script do Robot Framework.


- **Container com o Script do Robot Framework:**
  ```bash
  docker run -d -it --name robot-grid-poc --net grid localhost/robot-grid-poc:dev-0.0.1

#### c. Executando comando do Robot Framework dentro do Container

```bash
docker exec robot-grid-poc robot robot_selenium_grid.robot