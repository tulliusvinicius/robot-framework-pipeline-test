FROM python:3.10-bullseye

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY suite/ /app/suite
COPY page/ /app/page

CMD tail -f /dev/null
