*** Settings ***
Library            SeleniumLibrary
Resource           ../page/tests_page.robot
Test Setup         Abrir pagina


*** Test Cases ***

cenário 01 - cadastrar usuario com sucesso
    Dado estou na pagina de cadastro
    E que preencho os dados do usuario    Meg    meg@hotmail.com
    E clico no botao adicionar
    Entao verifico que o usuario foi cadastrado    Meg

cenário 02 - editar usuario com sucesso
    Dado estou na pagina de cadastro
    Quando clico em editar    Meg
    E que preencho os dados do usuario    Meg_Edit    meg_edit@hotmail.com
    E clico no botao atualizar
    Entao verifico que o usuario foi editado    Meg_Edit

cenário 03 - excluir usuario com sucesso
    Dado estou na pagina de cadastro
    Quando clico em excluir    Meg_Edit
    Entao verifico que o usuario foi excluido    Meg_Edit